package org.bwit.web.util;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.faces.context.FacesContext;

public class Messages {
	private static final String RESOURCE_NAME = "msgs";
	private static ResourceBundle bundle;

	public static ResourceBundle getBundle() {
		if (bundle == null) {
			final FacesContext context = FacesContext.getCurrentInstance();
			bundle = context.getApplication().getResourceBundle(context, RESOURCE_NAME);
		}
		return bundle;
	}

	public static String getValue(final String key, final String... params) {

		String result = null;
		try {
			result = getBundle().getString(key);
		} catch (final MissingResourceException e) {
			result = "???" + key + "???";
		}

		if (params != null) {
			final MessageFormat mf = new MessageFormat(result, getBundle().getLocale());
			result = mf.format(params, new StringBuffer(), null).toString();
		}

		return result;
	}
}
