package org.bwit.web.mb.base;

import java.util.Collection;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.bwit.importer.Importer;
import org.bwit.model.Entry;
import org.bwit.service.EntryService;
import org.bwit.web.mb.BaseMB;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;

@ViewScoped
@ManagedBean
public class ImporterMB extends BaseMB {

	@Autowired
	private Importer importer;

	@Autowired
	private EntryService entryService;

	public void importFile(){
		final Collection<Entry> importFile = importer.importFile("c:\\extrato.xlsx");
		System.out.println(importFile);
	}


	public void uploadFile(final FileUploadEvent event) {
		try {
			final UploadedFile file = event.getFile();
			final Collection<Entry> importFile = importer.importFile(file.getInputstream());
			System.out.println(importFile);
			entryService.save(importFile);
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}


}
