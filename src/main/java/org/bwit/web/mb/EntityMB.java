package org.bwit.web.mb;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;

import javax.annotation.PostConstruct;

import org.bwit.annotation.BeanBusiness;
import org.bwit.model.Entity;
import org.bwit.service.BaseService;
import org.bwit.web.util.Messages;

public abstract class EntityMB<T extends Entity, ID extends Serializable> extends BaseMB {

    private BaseService<T, ID> entityService;
    private T entity;

    @PostConstruct
    protected void preInit() {
        initBusinessEjbs();
        entity = entityService.createEntity();
        init();
    }

    private void initBusinessEjbs() {
        try {
            boolean beanBusinessFound = false;
            for (final Field field : this.getClass().getDeclaredFields()) {
                if (field.isAnnotationPresent(BeanBusiness.class)) {
                    field.setAccessible(true);
                    entityService = (BaseService<T,ID>) field.get(this);
                    beanBusinessFound = true;
                    break;
                }
            }
            if (!beanBusinessFound) {
                throw new IllegalStateException("No @BeanBusiness annotation found for this Managed Bean");
            }
        } catch (final SecurityException e) {
            throw new IllegalStateException(e);
        } catch (final IllegalAccessException e) {
            throw new IllegalStateException(e);
        }
    }

    public void init(){

    }

    public T getEntity() {
        return entity;
    }

    public void setEntity(final T entity) {
        this.entity = entity;
    }

    public List<T> getEntities() {
        return entityService.findAll();
    }

    public T save() {
        try {
			return entityService.save(entity);
		} catch (final Exception e) {
			return entity;
		}
    }

    public boolean isEditing() {
    	return entity.getId() != null;
    }

    public String getFieldsetTitle(){
    	final String resourceKey = isEditing() ? "app.edit" : "app.insert";
    	return Messages.getValue(resourceKey);
    }
}
