package org.bwit.web.mb.entity;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.bwit.annotation.BeanBusiness;
import org.bwit.model.Category;
import org.bwit.service.CategoryService;
import org.bwit.web.mb.EntityMB;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;

@ViewScoped
@ManagedBean
public class CategoriaMB extends EntityMB<Category, Integer> {

	@BeanBusiness
	@Autowired
	private CategoryService categoryService;

	private TreeNode root;
	private TreeNode selectedNode;

	public void init() {
		super.init();
	}

	public TreeNode getRoot(){
		return buildTree();
	}

	public TreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public Category save() {
		if (selectedNode != null && selectedNode.getData() != null) {
			getEntity().setRootCategory((Category) selectedNode.getData());
		}
		return super.save();
	}

	public void addCategory(){

	}

	public void addEditCategory(){

	}

	public void deleteCategory(){

	}

	private TreeNode buildTree() {
        root = new DefaultTreeNode("root", null);
        final List<Category> categoriasRoot = categoryService.findRootCategories();
        for (final Category categoria : categoriasRoot) {
            final TreeNode tnChild = new DefaultTreeNode(categoria, root);
            buildTreeRecursively(tnChild);
        }
        return root;
    }

    private void buildTreeRecursively(final TreeNode currentNode) {
    	final Category categoria = (Category) (currentNode.getData());
        for (final Category child : categoria.getChildren()) {
            final TreeNode tnChild = new DefaultTreeNode(child, currentNode);
            buildTreeRecursively(tnChild);
        }
    }





}
