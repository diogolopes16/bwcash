package org.bwit.web.mb;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

public abstract class BaseMB {

	protected void executeScript(final String script) {
		final RequestContext requestContext = RequestContext.getCurrentInstance();
		requestContext.execute(script);
	}

	protected static void addErrorMessage(final String formId, final String componentId, final String message) {
		final FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.addMessage(formId + ":" + componentId, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, message));
	}

}
