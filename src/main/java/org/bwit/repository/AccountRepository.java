package org.bwit.repository;

import org.bwit.model.hibernate.AccountHibernate;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends NamedBaseRepository<AccountHibernate, Integer> {

}
