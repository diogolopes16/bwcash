package org.bwit.repository;

import java.util.List;

import org.bwit.model.Category;
import org.bwit.model.hibernate.CategoryHibernate;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends NamedBaseRepository<CategoryHibernate, Integer> {

	public List<Category> findByRootCategoryIsNull();
}
