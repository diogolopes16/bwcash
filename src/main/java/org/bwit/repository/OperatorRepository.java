package org.bwit.repository;

import org.bwit.model.hibernate.OperatorHibernate;
import org.springframework.stereotype.Repository;

@Repository
public interface OperatorRepository extends NamedBaseRepository<OperatorHibernate, Integer> {

}
