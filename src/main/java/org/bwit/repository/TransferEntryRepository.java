package org.bwit.repository;

import org.bwit.model.hibernate.TransferEntryHibernate;
import org.springframework.stereotype.Repository;

@Repository
public interface TransferEntryRepository extends
		NamedBaseRepository<TransferEntryHibernate, Integer> {

}
