package org.bwit.repository;

import org.bwit.model.hibernate.IncomeEntryHibernate;
import org.springframework.stereotype.Repository;

@Repository
public interface IncomeEntryRepository extends NamedBaseRepository<IncomeEntryHibernate, Integer> {

}
