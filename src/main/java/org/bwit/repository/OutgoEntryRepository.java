package org.bwit.repository;

import org.bwit.model.hibernate.OutgoEntryHibernate;

public interface OutgoEntryRepository extends
	NamedBaseRepository<OutgoEntryHibernate, Integer> {

}
