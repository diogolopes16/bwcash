package org.bwit.repository;

import org.bwit.model.hibernate.BalanceEntryHibernate;
import org.springframework.stereotype.Repository;

@Repository
public interface BalanceEntryRepository extends NamedBaseRepository<BalanceEntryHibernate, Integer> {

}
