package org.bwit.repository;

import org.bwit.model.hibernate.PaymentMethodHibernate;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentMethodRepository extends
		NamedBaseRepository<PaymentMethodHibernate, Integer> {

}
