package org.bwit.repository;

import org.bwit.model.hibernate.EntryHibernate;
import org.springframework.stereotype.Repository;

@Repository
public interface EntryRepository extends NamedBaseRepository<EntryHibernate, Integer> {

}
