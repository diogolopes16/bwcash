package org.bwit.spring.configuration;

import java.io.IOException;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.bwit.spring.config.property.EntityManagerProperty;
import org.bwit.spring.config.property.HibernateProperty;
import org.bwit.spring.config.property.JNDIProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jndi.JndiTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Configure spring JPA application
 *
 * @author Carlos Belleza
 *
 */
@Configuration
@EnableTransactionManagement
public class RepositoryConfiguration {

    @Qualifier("hibernateProperty")
    @Autowired
    private HibernateProperty hibernateProperty;

    @Qualifier("entityManagerProperty")
    @Autowired
    private EntityManagerProperty entityManagerProperty;

    @Qualifier("jndiProperty")
    @Autowired
    private JNDIProperty jndiProperty;

    @Autowired
    private JndiTemplate jnditemplate;

    /**
     * Get Portal datasource
     *
     * @return DataSource
     * @throws NamingException
     * @throws IOException
     */
    @Bean
    public DataSource portalDataSource() throws NamingException, IOException {
        final String jndiDatasource = jndiProperty.getPortalDataSource();
        return (DataSource) jnditemplate.lookup(jndiDatasource);
    }

    /**
     * Get Portal entity manager
     *
     * @return LocalContainerEntityManagerFactoryBean
     * @throws NamingException
     * @throws IOException
     */
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws NamingException, IOException {
        final String[] entityManagerPackagesToScan = entityManagerProperty.getPackagesToScan();
        final String entityManagerPersistenceUnitName = entityManagerProperty.getPersistenceUnitName();
        final LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();

        if (!entityManagerPersistenceUnitName.isEmpty()) {
            entityManagerFactoryBean.setPersistenceUnitName(entityManagerPersistenceUnitName);
        }

        entityManagerFactoryBean.setDataSource(portalDataSource());
        entityManagerFactoryBean.setPackagesToScan(entityManagerPackagesToScan);
        entityManagerFactoryBean.setJpaVendorAdapter(hibernateProperty.getJpaVendorAdapter());
        entityManagerFactoryBean.setJpaPropertyMap(hibernateProperty.getJPAPropertyMap());

        return entityManagerFactoryBean;
    }

    /**
     * Get Portal transaction manager
     *
     * @return JpaTransactionManager
     * @throws ClassNotFoundException
     * @throws NamingException
     * @throws IOException
     */
    @Bean
    public JpaTransactionManager transactionManager() throws ClassNotFoundException, NamingException, IOException {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        transactionManager.setNestedTransactionAllowed(true);
        return transactionManager;
    }
}