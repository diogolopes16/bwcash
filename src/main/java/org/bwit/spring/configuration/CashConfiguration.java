package org.bwit.spring.configuration;

import org.bwit.spring.config.PropertySourcesConfig;
import org.bwit.spring.config.SecurityConfig;
import org.bwit.spring.config.StandardApplicationConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Configure Orca spring application
 *
 * @author Carlos Belleza
 *
 */
@Configuration(value = "cashConfiguration")
@ComponentScan("org.bwit")
@EnableJpaRepositories("org.bwit.repository")
@Import({ PropertySourcesConfig.class, StandardApplicationConfig.class, SecurityConfig.class,
        RepositoryConfiguration.class })
public class CashConfiguration {

}
