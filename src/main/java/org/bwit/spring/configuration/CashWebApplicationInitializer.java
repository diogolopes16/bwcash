package org.bwit.spring.configuration;

import org.bwit.spring.config.BaseWebApplicationInitializer;

public class CashWebApplicationInitializer extends
		BaseWebApplicationInitializer {

	protected Class<?> getRootConfigClass() {
		return CashConfiguration.class;
	}

}
