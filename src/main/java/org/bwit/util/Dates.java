package org.bwit.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Dates {

	public static String formatDate(final Date date){
		if (date != null) {
			final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			return sdf.format(date);
		}
		return null;
	}


}
