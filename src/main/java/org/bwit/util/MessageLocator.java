package org.bwit.util;

import java.util.Locale;

import org.springframework.context.MessageSource;

public interface MessageLocator {

    public abstract void setMessageSource(MessageSource messageSource);

    /**
     * Get default locale
     *
     * @return Locale
     */
    public abstract Locale getDefaultLocale();

    /**
     * Set default locale
     *
     * @param defaultLocale
     */
    public abstract void setDefaultLocale(Locale defaultLocale);

    /**
     * Get application message with default locale
     *
     * @param key
     * @return String
     */
    public abstract String getMessage(String key);

    /**
     * Get application message with specific locale
     *
     * @param key
     * @param locale
     * @return String
     */
    public abstract String getMessage(String key, Locale locale);

    /**
     * Get application message with specific locale
     *
     * @param key
     * @param locale
     * @return String
     */
    public abstract String getMessage(String key, String defaultMessage, Locale locale);

    /**
     * Get application message with specific locale and arguments
     *
     * @param key
     * @param args
     * @param locale
     * @return String
     */
    public abstract String getMessage(String key, Object args[], Locale locale);

    /**
     * Get application message with specific locale and arguments
     *
     * @param key
     * @param args
     * @param defaultMessage
     * @param locale
     * @return String
     */
    public abstract String getMessage(String key, Object args[], String defaultMessage, Locale locale);

}