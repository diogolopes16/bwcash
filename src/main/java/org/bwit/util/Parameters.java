package org.bwit.util;

import java.util.HashMap;

public class Parameters extends HashMap<String, Object> {

	private static final long serialVersionUID = 5590216368828218555L;

	public Parameters put(final String key, final Object value) {
		super.put(key, value);
		return this;
	}
	
}