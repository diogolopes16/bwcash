package org.bwit.layout;

import java.math.BigDecimal;
import java.util.Date;

import org.bwit.annotation.LayoutManager;
import org.bwit.annotation.RowIndex;
import org.bwit.model.Entry;

import com.google.common.base.Strings;

@LayoutManager(startRow=1)
public class ItauCheckingAccountLayout implements Layout {

	@RowIndex(0)
	private Date date;
	@RowIndex(1)
	private String description;
	@RowIndex(2)
	private String internalCode;
	@RowIndex(3)
	private BigDecimal value;
	@RowIndex(4)
	private Boolean credit;
	@RowIndex(5)
	private String balanceText;
	@RowIndex(6)
	private Boolean balanceCredit;

	public BigDecimal getValue() {
		if (isBalance()) {
			final int signal = balanceCredit ? 1 : -1;
			return new BigDecimal(balanceText).multiply(BigDecimal.valueOf(signal));
		} else if (value != null) {
			final int signal = credit ? 1 : -1;
			return value.multiply(BigDecimal.valueOf(signal));
		}
		return null;
	}

	public Boolean isCredit() {
		return credit;
	}

	public Boolean isBalance() {
		return !Strings.isNullOrEmpty(balanceText);
	}

	public Date getDate() {
		return date;
	}

	public String getDescription() {
		return description;
	}

	public String getInternalCode() {
		return internalCode;
	}

	public Entry copyToEntity(final Entry lancamento) {
		lancamento.setPaymentDate(getDate());
		lancamento.setName(getDescription());
		lancamento.setInternalCode(getInternalCode());
		lancamento.setValue(getValue());
		return lancamento;
	}

}
