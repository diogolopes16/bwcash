package org.bwit.layout;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.bwit.annotation.LayoutManager;
import org.bwit.annotation.RowIndex;

public class LayoutConverter {

	public static <L extends Layout> L convertType(final Class<L> layoutClass,
			final Row row) {
		return convert(layoutClass, row, true);
	}

	public static <L extends Layout> L convertString(
			final Class<L> layoutClass, final Row row) {
		return convert(layoutClass, row, false);
	}

	private static <L extends Layout> L convert(final Class<L> layoutClass,
			final Row row, final Boolean usingTypes) {
		try {
			final L layouImpl = layoutClass.newInstance();
			final LayoutManager layoutManager = layoutClass
					.getAnnotation(LayoutManager.class);
			final Field[] fields = layoutClass.getDeclaredFields();
			int index = 0;

			if (layoutManager != null) {
				int startRow = layoutManager.startRow();
				if (row.getRowNum() < startRow) {
					return null;
				}
			}

			if (row.getCell(0).getDateCellValue() == null) {
				return null;
			}

			for (final Field field : fields) {
				field.setAccessible(true);
				if (field.isAnnotationPresent(RowIndex.class)) {
					final RowIndex rowIndex = field
							.getAnnotation(RowIndex.class);
					index = rowIndex.value();
				}
				final Cell cell = row.getCell(index++);
				field.set(layouImpl, usingTypes ? getCelTypeValue(field, cell)
						: getCelStringValue(cell));
			}
			return layouImpl;

		} catch (final InstantiationException | IllegalAccessException e) {
			return null;
		}
	}

	private static Object getCelStringValue(final Cell cell) {
		return cell != null ? getStringCellValue(cell) : "NULL";
	}

	private static String getStringCellValue(final Cell cell) {
		cell.setCellType(Cell.CELL_TYPE_STRING);
		final String stringCellValue = cell.getStringCellValue();
		return stringCellValue != null ? stringCellValue.trim() : "";
	}

	private static Object getCelTypeValue(final Field field, final Cell cell) {
		final Class<?> type = field.getType();

		if (Number.class.isAssignableFrom(type)) {
			final Double cellValue = getCellDoubleValue(cell);
			if (cellValue == null) {
				return null;
			} else if (Integer.class.isAssignableFrom(type)) {
				return cellValue.intValue();
			} else if (Long.class.isAssignableFrom(type)) {
				return cellValue.longValue();
			} else if (Float.class.isAssignableFrom(type)) {
				return cellValue.floatValue();
			} else if (Double.class.isAssignableFrom(type)) {
				return cellValue.doubleValue();
			} else if (Short.class.isAssignableFrom(type)) {
				return cellValue.shortValue();
			} else if (BigDecimal.class.isAssignableFrom(type)) {
				return new BigDecimal(cellValue);
			}
		} else if (Date.class.isAssignableFrom(type)) {
			return cell.getDateCellValue();
		} else if (Boolean.class.isAssignableFrom(type)) {
			final String stringCellValue = getStringCellValue(cell);
			return !"-".equals(stringCellValue);
		}

		return getCelStringValue(cell);
	}

	private static Double getCellDoubleValue(final Cell cell) {
		try {
			return cell.getNumericCellValue();
		} catch (final IllegalStateException e) {
			final String stringCellValue = cell.getStringCellValue().trim();

			if (StringUtils.isBlank(stringCellValue)
					|| !StringUtils.isNumeric(stringCellValue)) {
				return null;
			}
			return Double.valueOf(stringCellValue);
		}
	}
}
