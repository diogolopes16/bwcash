package org.bwit.layout;

import java.math.BigDecimal;
import java.util.Date;

import org.bwit.model.Entry;

public interface Layout {

	public Boolean isCredit();

	public Boolean isBalance();

	public Date getDate();

	public String getDescription();

	public String getInternalCode();

	public BigDecimal getValue();

	public Entry copyToEntity(final Entry entry);
}
