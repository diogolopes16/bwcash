package org.bwit.model;

import java.util.Set;


public interface Category extends NamedEntity<Long> {

	public Set<Category> getChildren();
	public void setChildren(final Set<Category> children);

	public Category getRootCategory();
	public void setRootCategory(final Category rootCategory);
}
