package org.bwit.model.hibernate;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.bwit.model.OutgoEntry;

@Entity(name = "OutgoEntry")
@DiscriminatorValue("O")
public class OutgoEntryHibernate extends EntryHibernate implements OutgoEntry {

    private static final long serialVersionUID = 1458152946926005333L;

}
