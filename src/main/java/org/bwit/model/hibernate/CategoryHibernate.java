package org.bwit.model.hibernate;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.bwit.model.Category;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity(name = "Category")
@Table(name = "category")
public class CategoryHibernate extends HibernateNamedEntity<Long> implements Category {

    private static final long serialVersionUID = -5110230698091247112L;

    @Fetch(FetchMode.JOIN)
    @OneToMany(mappedBy = "rootCategory",
            fetch = FetchType.EAGER,
            targetEntity = CategoryHibernate.class)
    private Set<Category> children;

    @ManyToOne(targetEntity = CategoryHibernate.class)
    @JoinColumn(name = "root_category_id")
    private Category rootCategory;

    public Set<Category> getChildren() {
        return children;
    }

    public void setChildren(final Set<Category> children) {
        this.children = children;
    }

    public Category getRootCategory() {
        return rootCategory;
    }

    public void setRootCategory(final Category categoriaPai) {
        this.rootCategory = categoriaPai;
    }

}
