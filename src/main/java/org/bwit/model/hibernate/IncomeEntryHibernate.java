package org.bwit.model.hibernate;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.bwit.model.IncomeEntry;

@Entity(name = "IncomeEntry")
@DiscriminatorValue("I")
public class IncomeEntryHibernate extends EntryHibernate implements IncomeEntry {

    private static final long serialVersionUID = 1458152946926005333L;

}
