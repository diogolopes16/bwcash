package org.bwit.model.hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.bwit.model.Category;
import org.bwit.model.CategoryAssociate;

@Entity(name = "CategoryAssociate")
@Table(name = "category_associate")
public class CategoryAssociateHibernate extends HibernateNamedEntity<Long> implements CategoryAssociate {

    private static final long serialVersionUID = -5110230698091247112L;

    @Column
    private Boolean regex;

    @ManyToOne(targetEntity = CategoryHibernate.class)
    @JoinColumn(name = "category_id")
    private Category category;

    public Boolean isRegex() {
        return regex;
    }

    public void setRegex(final Boolean regex) {
        this.regex = regex;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(final Category category) {
        this.category = category;
    }

}
