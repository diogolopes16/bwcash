package org.bwit.model.hibernate;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.bwit.model.Account;
import org.bwit.model.Category;
import org.bwit.model.Entry;
import org.bwit.model.Operator;
import org.bwit.model.PaymentMethod;
import org.bwit.model.PeriodType;
import org.bwit.util.Dates;

import com.google.common.base.Objects;

@Entity(name = "Entry")
@Inheritance
@DiscriminatorColumn(name = "entryType")
@Table(name = "entry")
public abstract class EntryHibernate extends HibernateNamedEntity<Long> implements Entry {

    private static final long serialVersionUID = -9066224799104143218L;

    @Column
    private Date paymentDate;

    @ManyToOne(fetch = FetchType.EAGER,
            targetEntity = CategoryHibernate.class)
    @JoinColumn(name = "category_id")
    private Category category;

    @ManyToOne(fetch = FetchType.EAGER,
            targetEntity = PaymentMethodHibernate.class)
    @JoinColumn(name = "payment_method_id",
            nullable = false)
    private PaymentMethod paymentMethod;

    @ManyToOne(fetch = FetchType.EAGER,
            targetEntity = OperatorHibernate.class)
    @JoinColumn(name = "operator_id",
            nullable = false)
    private Operator operator;

    @Column
    @Enumerated(EnumType.STRING)
    private PeriodType periodType;

    @ManyToOne(fetch = FetchType.EAGER,
            targetEntity = AccountHibernate.class)
    @JoinColumn(name = "account_id",
            nullable = false)
    private Account account;

    @Column
    private BigDecimal value;

    @Column
    private String internalCode;

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public PeriodType getPeriodType() {
        return periodType;
    }

    public void setPeriodType(PeriodType periodType) {
        this.periodType = periodType;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getInternalCode() {
        return internalCode;
    }

    public void setInternalCode(String internalCode) {
        this.internalCode = internalCode;
    }

    public String toString() {
        return Objects.toStringHelper(this.getClass()).addValue(Dates.formatDate(paymentDate)).addValue(getName())
                .addValue(value).toString();
    }

}
