package org.bwit.model.hibernate;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.bwit.model.PaymentMethod;

@Entity(name = "PaymentMethod")
@Table(name = "payment_method")
public class PaymentMethodHibernate extends HibernateNamedEntity<Long> implements PaymentMethod {

    private static final long serialVersionUID = 7074538344184845223L;

}
