package org.bwit.model.hibernate;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.bwit.model.BalanceEntry;

@Entity(name = "BalanceEntry")
@DiscriminatorValue("B")
public class BalanceEntryHibernate extends EntryHibernate implements BalanceEntry {

    private static final long serialVersionUID = 1458152946926005333L;

}
