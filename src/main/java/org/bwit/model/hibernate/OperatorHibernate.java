package org.bwit.model.hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.bwit.model.Operator;

@Entity(name = "Operator")
@Table(name = "operador")
public class OperatorHibernate extends HibernateNamedEntity<Long> implements Operator {

    private static final long serialVersionUID = 4709722657004593546L;

    @Column
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

}
