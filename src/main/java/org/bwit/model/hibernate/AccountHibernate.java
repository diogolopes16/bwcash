package org.bwit.model.hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import org.bwit.model.Account;
import org.bwit.model.AccountType;

@Entity(name = "Account")
@Table(name = "account")
public class AccountHibernate extends HibernateNamedEntity<Long> implements Account {

    private static final long serialVersionUID = 5194622345591527829L;

    @Column
    @Enumerated(EnumType.STRING)
    private AccountType accountType;

    public AccountType getTipoConta() {
        return accountType;
    }

    public void setTipoConta(final AccountType accountType) {
        this.accountType = accountType;
    }

}
