package org.bwit.model.hibernate;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.bwit.model.Account;
import org.bwit.model.TransferEntry;
import org.bwit.model.TransferType;

@Entity(name = "TransferEntry")
@DiscriminatorValue("T")
public class TransferEntryHibernate extends EntryHibernate implements TransferEntry {

    private static final long serialVersionUID = 1458152946926005333L;

    @ManyToOne(fetch = FetchType.EAGER,
            targetEntity = AccountHibernate.class)
    @JoinColumn(name = "transfer_account_id",
            nullable = true)
    private Account transferAccount;

    @Column
    @Enumerated(EnumType.STRING)
    private TransferType transferType;

    public Account getTransferAccount() {
        return transferAccount;
    }

    public void setTransferAccount(final Account transferAccount) {
        this.transferAccount = transferAccount;
    }

    public TransferType getTransferType() {
        return transferType;
    }

    public void setTransferType(final TransferType transferType) {
        this.transferType = transferType;
    }

}
