package org.bwit.model;

public enum AccountType {
	CORRENTE, CREDITO, INVESTIMENTO;
}
