package org.bwit.model;

public interface Account extends NamedEntity<Long> {

    public AccountType getTipoConta();

    public void setTipoConta(final AccountType accountType);

}
