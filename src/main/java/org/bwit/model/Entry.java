package org.bwit.model;

import java.math.BigDecimal;
import java.util.Date;

public interface Entry extends NamedEntity<Long> {

    public Date getPaymentDate();
    public void setPaymentDate(final Date paymentDate);

    public Category getCategory();
    public void setCategory(final Category category);

    public PaymentMethod getPaymentMethod();
    public void setPaymentMethod(final PaymentMethod paymentMethod);

    public BigDecimal getValue();
    public void setValue(final BigDecimal value);

    public Operator getOperator();
    public void setOperator(final Operator operator);

    public PeriodType getPeriodType();
    public void setPeriodType(final PeriodType periodType);

    public Account getAccount();
    public void setAccount(final Account account);

    public String getInternalCode();
    public void setInternalCode(final String internalCode);
}
