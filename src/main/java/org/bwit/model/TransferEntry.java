package org.bwit.model;


public interface TransferEntry extends Entry {

	public Account getTransferAccount();
	public void setTransferAccount(final Account transferAccount);

	public TransferType getTransferType();
	public void setTransferType(final TransferType transferType);
}
