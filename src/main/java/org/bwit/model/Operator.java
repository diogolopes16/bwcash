package org.bwit.model;

public interface Operator extends NamedEntity<Long> {

	public String getEmail();
	public void setEmail(final String email);
}
