package org.bwit.model;



public interface CategoryAssociate extends NamedEntity<Long> {

	public Boolean isRegex();
	public void setRegex(final Boolean isRegex);

	public Category getCategory();
	public void setCategory(final Category category);
}
