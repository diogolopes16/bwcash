package org.bwit.model;

public enum PeriodType {
	BIMONTHLY(15),
	MONTHLY(30);

	private final Integer periodo;

	PeriodType(final Integer periodo){
		this.periodo = periodo;
	}

	public Integer getPeriodo(){
		return this.periodo;
	}
}
