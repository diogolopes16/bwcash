package org.bwit.service;

import org.bwit.model.Operator;

public interface OperatorService extends NamedBaseService<Operator, Integer> {

}
