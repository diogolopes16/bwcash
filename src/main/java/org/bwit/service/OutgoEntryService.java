package org.bwit.service;

import org.bwit.model.OutgoEntry;

public interface OutgoEntryService extends NamedBaseService<OutgoEntry, Integer> {

}
