package org.bwit.service;

import java.util.List;

import org.bwit.model.Category;

public interface CategoryService extends NamedBaseService<Category, Integer> {

    public List<Category> findRootCategories();
}
