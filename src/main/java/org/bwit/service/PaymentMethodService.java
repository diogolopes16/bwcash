package org.bwit.service;

import org.bwit.model.PaymentMethod;

public interface PaymentMethodService extends NamedBaseService<PaymentMethod, Integer> {

}
