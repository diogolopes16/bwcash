package org.bwit.service;

import org.bwit.model.BalanceEntry;

public interface BalanceEntryService extends NamedBaseService<BalanceEntry, Integer> {

}
