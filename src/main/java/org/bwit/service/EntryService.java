package org.bwit.service;

import org.bwit.layout.Layout;
import org.bwit.model.Entry;

public interface EntryService extends NamedBaseService<Entry, Integer> {

    public Entry createEntry(final Layout layout);
}
