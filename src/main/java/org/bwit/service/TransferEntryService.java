package org.bwit.service;

import org.bwit.model.TransferEntry;

public interface TransferEntryService extends NamedBaseService<TransferEntry, Integer> {

}
