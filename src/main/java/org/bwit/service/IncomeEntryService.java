package org.bwit.service;

import org.bwit.model.IncomeEntry;

public interface IncomeEntryService extends NamedBaseService<IncomeEntry, Integer> {

}
