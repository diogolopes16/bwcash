package org.bwit.service.hibernate;

import org.bwit.layout.Layout;
import org.bwit.model.Entry;
import org.bwit.model.hibernate.EntryHibernate;
import org.bwit.repository.EntryRepository;
import org.bwit.repository.NamedBaseRepository;
import org.bwit.service.BalanceEntryService;
import org.bwit.service.EntryService;
import org.bwit.service.IncomeEntryService;
import org.bwit.service.OutgoEntryService;
import org.bwit.service.TransferEntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class EntryServiceHibernate extends NamedBaseServiceHibernate<Entry, Integer> implements EntryService {

    @Autowired
    private EntryRepository repository;

    @Autowired
    private OutgoEntryService outgoEntryService;

    @Autowired
    private IncomeEntryService incomeEntryService;

    @Autowired
    private BalanceEntryService balanceEntryService;

    @Autowired
    private TransferEntryService transferEntryService;

    public EntryServiceHibernate() {
        super(EntryHibernate.class);
    }

    protected NamedBaseRepository getRepository() {
        return repository;
    }

    public Entry createEntry(final Layout layout) {
        final Entry entry = createRightEntry(layout);
        return layout.copyToEntity(entry);
    }

    private Entry createRightEntry(final Layout layout) {
        return layout.isBalance() ? balanceEntryService.createEntity() : createCreditEntry(layout);
    }

    private Entry createCreditEntry(final Layout layout) {
        return layout.isCredit() ? incomeEntryService.createEntity() : outgoEntryService.createEntity();
    }

}
