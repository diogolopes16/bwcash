package org.bwit.service.hibernate;

import org.bwit.model.Account;
import org.bwit.model.hibernate.AccountHibernate;
import org.bwit.repository.AccountRepository;
import org.bwit.repository.NamedBaseRepository;
import org.bwit.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class AccountServiceHibernate extends NamedBaseServiceHibernate<Account, Integer> implements AccountService {

    @Autowired
    private AccountRepository repository;

    public AccountServiceHibernate() {
        super(AccountHibernate.class);
    }

    protected NamedBaseRepository getRepository() {
        return repository;
    }

}
