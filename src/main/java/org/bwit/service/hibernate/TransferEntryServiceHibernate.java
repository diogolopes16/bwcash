package org.bwit.service.hibernate;

import org.bwit.model.TransferEntry;
import org.bwit.model.hibernate.TransferEntryHibernate;
import org.bwit.repository.NamedBaseRepository;
import org.bwit.repository.TransferEntryRepository;
import org.bwit.service.TransferEntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class TransferEntryServiceHibernate extends NamedBaseServiceHibernate<TransferEntry, Integer> implements
        TransferEntryService {

    @Autowired
    private TransferEntryRepository repository;

    public TransferEntryServiceHibernate() {
        super(TransferEntryHibernate.class);
    }

    protected NamedBaseRepository getRepository() {
        return repository;
    }

}
