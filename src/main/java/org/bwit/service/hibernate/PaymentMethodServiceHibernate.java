package org.bwit.service.hibernate;

import org.bwit.model.PaymentMethod;
import org.bwit.model.hibernate.PaymentMethodHibernate;
import org.bwit.repository.NamedBaseRepository;
import org.bwit.repository.OperatorRepository;
import org.bwit.service.PaymentMethodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class PaymentMethodServiceHibernate extends NamedBaseServiceHibernate<PaymentMethod, Integer> implements
        PaymentMethodService {

    @Autowired
    private OperatorRepository repository;

    public PaymentMethodServiceHibernate() {
        super(PaymentMethodHibernate.class);
    }

    protected NamedBaseRepository getRepository() {
        return repository;
    }

}
