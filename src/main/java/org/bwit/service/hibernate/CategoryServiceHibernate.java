package org.bwit.service.hibernate;

import java.util.List;

import org.bwit.model.Category;
import org.bwit.model.hibernate.CategoryHibernate;
import org.bwit.repository.CategoryRepository;
import org.bwit.repository.NamedBaseRepository;
import org.bwit.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class CategoryServiceHibernate extends NamedBaseServiceHibernate<Category, Integer> implements CategoryService {

    @Autowired
    private CategoryRepository repository;

    public CategoryServiceHibernate() {
        super(CategoryHibernate.class);
    }

    protected NamedBaseRepository getRepository() {
        return repository;
    }

    public List<Category> findRootCategories() {
        return repository.findByRootCategoryIsNull();
    }

}
