package org.bwit.service.hibernate;

import org.bwit.model.Operator;
import org.bwit.model.hibernate.OperatorHibernate;
import org.bwit.repository.NamedBaseRepository;
import org.bwit.repository.OperatorRepository;
import org.bwit.service.OperatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class OperatorServiceHibernate extends NamedBaseServiceHibernate<Operator, Integer> implements OperatorService {

    @Autowired
    private OperatorRepository repository;

    public OperatorServiceHibernate() {
        super(OperatorHibernate.class);
    }

    protected NamedBaseRepository getRepository() {
        return repository;
    }

}
