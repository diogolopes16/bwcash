package org.bwit.service.hibernate;

import org.bwit.model.IncomeEntry;
import org.bwit.model.hibernate.IncomeEntryHibernate;
import org.bwit.repository.IncomeEntryRepository;
import org.bwit.repository.NamedBaseRepository;
import org.bwit.service.IncomeEntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class IncomeEntryServiceHibernate extends NamedBaseServiceHibernate<IncomeEntry, Integer> implements
        IncomeEntryService {

    @Autowired
    private IncomeEntryRepository repository;

    public IncomeEntryServiceHibernate() {
        super(IncomeEntryHibernate.class);
    }

    protected NamedBaseRepository getRepository() {
        return repository;
    }

}
