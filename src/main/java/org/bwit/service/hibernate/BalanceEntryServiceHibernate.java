package org.bwit.service.hibernate;

import org.bwit.model.BalanceEntry;
import org.bwit.model.hibernate.BalanceEntryHibernate;
import org.bwit.repository.BalanceEntryRepository;
import org.bwit.repository.NamedBaseRepository;
import org.bwit.service.BalanceEntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class BalanceEntryServiceHibernate extends NamedBaseServiceHibernate<BalanceEntry, Integer> implements
        BalanceEntryService {

    @Autowired
    private BalanceEntryRepository repository;

    public BalanceEntryServiceHibernate() {
        super(BalanceEntryHibernate.class);
    }

    protected NamedBaseRepository getRepository() {
        return repository;
    }

}
