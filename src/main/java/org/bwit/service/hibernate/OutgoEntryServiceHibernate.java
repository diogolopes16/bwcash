package org.bwit.service.hibernate;

import org.bwit.model.OutgoEntry;
import org.bwit.model.hibernate.OutgoEntryHibernate;
import org.bwit.repository.NamedBaseRepository;
import org.bwit.repository.OutgoEntryRepository;
import org.bwit.service.OutgoEntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class OutgoEntryServiceHibernate extends NamedBaseServiceHibernate<OutgoEntry, Integer> implements
        OutgoEntryService {

    @Autowired
    private OutgoEntryRepository repository;

    public OutgoEntryServiceHibernate() {
        super(OutgoEntryHibernate.class);
    }

    protected NamedBaseRepository getRepository() {
        return repository;
    }

}
