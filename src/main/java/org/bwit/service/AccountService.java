package org.bwit.service;

import org.bwit.model.Account;

public interface AccountService extends NamedBaseService<Account, Integer> {

}
