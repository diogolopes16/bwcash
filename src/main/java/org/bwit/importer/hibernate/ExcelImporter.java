package org.bwit.importer.hibernate;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.bwit.factory.EntityFactory;
import org.bwit.importer.Importer;
import org.bwit.layout.ItauCheckingAccountLayout;
import org.bwit.layout.Layout;
import org.bwit.layout.LayoutConverter;
import org.bwit.model.Account;
import org.bwit.model.Category;
import org.bwit.model.Entry;
import org.bwit.model.Operator;
import org.bwit.model.PaymentMethod;
import org.bwit.service.EntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExcelImporter implements Importer {

    @Autowired
    private EntryService entryService;

    @Autowired
    private EntityFactory entityFactory;

    public Collection<Entry> importFile(final InputStream fileInputStream) {
        try {
            final Workbook wb = WorkbookFactory.create(fileInputStream);
            return parseLayout(wb.getSheetAt(0), ItauCheckingAccountLayout.class);

        } catch (InvalidFormatException | IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public Collection<Entry> importFile(final String fileName) {
        try {
            final Workbook wb = WorkbookFactory.create(new File(fileName));
            return parseLayout(wb.getSheetAt(0), ItauCheckingAccountLayout.class);

        } catch (InvalidFormatException | IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private <L extends Layout> Collection<Entry> parseLayout(final Sheet sheet, final Class<L> layoutClass) {
        final Collection<Entry> entries = new ArrayList<Entry>();
        final Account account = entityFactory.createAccount();
        final Category category = entityFactory.createCategory();
        final Operator operator = entityFactory.createOperator();
        final PaymentMethod paymentMethod = entityFactory.createPaymentMethod();

        for (final Row row : sheet) {
            final L layoutConverted = LayoutConverter.convertType(layoutClass, row);
            if (layoutConverted != null) {
                final Entry createEntity = entryService.createEntry(layoutConverted);
                createEntity.setAccount(account);
                createEntity.setCategory(category);
                createEntity.setOperator(operator);
                createEntity.setPaymentMethod(paymentMethod);

                entries.add(createEntity);
            }
        }

        try {
            entryService.save(entries);
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return entries;
    }

    /*
     * private Entry criarLancamento(final Row row) {
     *
     * final ItauCheckingAccountLayout lancamentoWrapper =
     * ItauCheckingAccountLayout .create(); lancamentoWrapper
     * .date(row.getCell(COLUNA_DATA).getDateCellValue())
     * .descricao(row.getCell(COLUNA_DESCRICAO).getStringCellValue())
     * .codigoInterno(row.getCell(COLUNA_CODIGO_INTERNO).toString())
     * .saldo(row.getCell(COLUNA_SALDO).toString(),
     * row.getCell(COLUNA_SALDO).getNumericCellValue())
     * .credito(isBlankCell(row.getCell(COLUNA_SINAL)))
     * .valor(row.getCell(COLUNA_VALOR).getNumericCellValue());
     *
     * return lancamentoBusiness.createEntry(lancamentoWrapper); }
     *
     * private boolean isBlankCell(final Cell cell) { return (cell == null ||
     * cell.getCellType() == Cell.CELL_TYPE_BLANK); }
     */

}
