package org.bwit.importer;

import java.io.InputStream;
import java.util.Collection;

import org.bwit.model.Entry;

public interface Importer {

	public static final String CONTA_ITAU_EXCEL_IMPORTER = "contaItauExcel";
	public static final String CREDITO_ITAU_EXCEL_IMPORTER = "creditoItauExcel";

	public Collection<Entry> importFile(final InputStream fileInputStream);
	public Collection<Entry> importFile(final String fileName);
}
