package org.bwit.factory;

import org.bwit.model.Account;
import org.bwit.model.Category;
import org.bwit.model.Operator;
import org.bwit.model.PaymentMethod;

public interface EntityFactory {

	public Category createCategory();
	public Operator createOperator();
	public PaymentMethod createPaymentMethod();
	public Account createAccount();

}