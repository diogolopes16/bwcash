package org.bwit.factory.hibernate;

import java.util.Random;

import org.bwit.factory.Factory;
import org.bwit.model.PaymentMethod;
import org.bwit.service.PaymentMethodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public final class PaymentMethodFactory implements Factory<PaymentMethod> {
    private static final Random random = new Random();

    @Autowired
    private PaymentMethodService paymentMethodService;

    public enum TipoMeioPagamento {
        CARTAO("Cartão"),
        DEBITO("Débito"),
        DINHEIRO("Dinheiro"),
        BOLETO("Boleto"),
        DEBITO_AUTOMATICO("Débito Automático");

        private final String nome;

        TipoMeioPagamento(final String nome) {
            this.nome = nome;
        }

        public String getNome() {
            return nome;
        }
    }

    public final PaymentMethod getMeioPagamento(final TipoMeioPagamento tipoMeioPagamento) {
        final PaymentMethod meioPagamento = paymentMethodService.findByName(tipoMeioPagamento.getNome());
        if (meioPagamento == null) {
            return createNewMeioPagamento(tipoMeioPagamento);
        }
        return meioPagamento;
    }

    private PaymentMethod createNewMeioPagamento(final TipoMeioPagamento tipoMeioPagamento) {
        try {
            final PaymentMethod meioPagamento = paymentMethodService.createEntity();
            meioPagamento.setName(tipoMeioPagamento.getNome());
            return paymentMethodService.save(meioPagamento);
        } catch (final Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PaymentMethod create() {
        final TipoMeioPagamento[] values = TipoMeioPagamento.values();
        return getMeioPagamento(values[random.nextInt(values.length)]);
    }

}
