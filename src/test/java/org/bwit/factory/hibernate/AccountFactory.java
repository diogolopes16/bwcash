package org.bwit.factory.hibernate;

import java.util.Random;

import org.bwit.factory.Factory;
import org.bwit.model.Account;
import org.bwit.model.AccountType;
import org.bwit.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AccountFactory implements Factory<Account> {

	@Autowired
	private AccountService accountService;

	private static final Random random = new Random();

	private Account getAccount(final AccountType accountType) {
		if (accountType == null) {
			return null;
		}

		final Account account = this.accountService.findByName("Account " + accountType);
		if (account == null) {
			return createNewAccount(accountType);
		}
		return account;
	}

	private Account createNewAccount(final AccountType accountType) {
		try {
			final Account account = accountService.createEntity();
			account.setName("Account " + accountType);
			account.setTipoConta(accountType);

			return accountService.save(account);
		} catch (final Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Account create() {
		final AccountType[] values = AccountType.values();
		return getAccount(values[random.nextInt(values.length)]);
	}

}
