package org.bwit.factory.hibernate;

import java.util.Random;

import org.bwit.factory.Factory;
import org.bwit.model.Operator;
import org.bwit.service.OperatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public final class OperatorFactory implements Factory<Operator> {

    private static final Random random = new Random();

    @Autowired
    private OperatorService operatorService;

    public enum TipoOperador {
        DIOGO("Diogo", "diogolopes@gmail.com"),
        TATHIANA("Tathiana", "tathimf@hotmail.com");

        private final String nome;
        private final String email;

        TipoOperador(final String nome, final String email) {
            this.nome = nome;
            this.email = email;
        }

        public String getNome() {
            return nome;
        }

        public String getEmail() {
            return email;
        }

    }

    public final Operator getOperador(final TipoOperador tipoOperador) {
        final Operator operador = operatorService.findByName(tipoOperador.getNome());
        if (operador == null) {
            return createNewOperator(tipoOperador);
        }
        return operador;
    }

    private Operator createNewOperator(final TipoOperador tipoOperador) {
        try {
            final Operator operador = operatorService.createEntity();
            operador.setName(tipoOperador.getNome());
            operador.setEmail(tipoOperador.getEmail());
            return operatorService.save(operador);
        } catch (final Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Operator create() {
        final TipoOperador[] values = TipoOperador.values();
        return getOperador(values[random.nextInt(values.length)]);

    }

}
