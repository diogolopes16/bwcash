package org.bwit.factory.hibernate;

import java.util.Random;

import org.bwit.factory.Factory;
import org.bwit.model.Category;
import org.bwit.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CategoryFactory implements Factory<Category> {

    @Autowired
    private CategoryService categoryService;

    private static final Random random = new Random();

    public enum TipoCategoria {
        CASA("Casa", null),
        CARRO("Carro", null),
        ALIMENTACAO("Alimentação", null),
        GASOLINA("Gasolina", CARRO),
        ALMOCO("Almoço", ALIMENTACAO);

        private final String nome;
        private final TipoCategoria categoriaPai;

        TipoCategoria(final String nome, final TipoCategoria categoriaPai) {
            this.nome = nome;
            this.categoriaPai = categoriaPai;
        }

        public String getNome() {
            return nome;
        }

        public TipoCategoria getCategoriaPai() {
            return categoriaPai;
        }

    }

    public final Category getCategoria(final TipoCategoria tipoCategoria) {
        if (tipoCategoria == null) {
            return null;
        }

        final Category categoria = categoryService.findByName(tipoCategoria.getNome());
        if (categoria == null) {
            return createNewCategoria(tipoCategoria);
        }
        return categoria;
    }

    private Category createNewCategoria(final TipoCategoria tipoCategoria) {
        try {
            final Category category = categoryService.createEntity();
            category.setName(tipoCategoria.getNome());
            category.setRootCategory(getCategoria(tipoCategoria.getCategoriaPai()));
            return categoryService.save(category);
        } catch (final Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Category create() {
        final TipoCategoria[] values = TipoCategoria.values();
        return getCategoria(values[random.nextInt(values.length)]);
    }

}
