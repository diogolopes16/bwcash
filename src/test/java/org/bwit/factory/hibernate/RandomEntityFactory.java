package org.bwit.factory.hibernate;

import org.bwit.factory.EntityFactory;
import org.bwit.factory.Factory;
import org.bwit.model.Account;
import org.bwit.model.Category;
import org.bwit.model.Operator;
import org.bwit.model.PaymentMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RandomEntityFactory implements EntityFactory  {

	@Autowired
	private Factory<Category> categoryFactory;

	@Autowired
	private Factory<Operator> operatorFactory;

	@Autowired
	private Factory<PaymentMethod> paymentMethodFactory;

	@Autowired
	private Factory<Account> accountFactory;

	public Category createCategory() {
		return categoryFactory.create();
	}

	public Operator createOperator() {
		return operatorFactory.create();
	}

	public PaymentMethod createPaymentMethod() {
		return paymentMethodFactory.create();
	}

	public Account createAccount() {
		return accountFactory.create();
	}

}
