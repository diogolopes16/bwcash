package org.bwit.factory.hibernate;

import java.math.BigDecimal;
import java.util.Date;

import org.bwit.factory.Factory;
import org.bwit.model.Category;
import org.bwit.model.Entry;
import org.bwit.model.Operator;
import org.bwit.model.OutgoEntry;
import org.bwit.model.PaymentMethod;
import org.bwit.model.PeriodType;
import org.bwit.model.hibernate.OutgoEntryHibernate;
import org.bwit.service.OutgoEntryService;
import org.springframework.stereotype.Component;

@Component
public class OutgoEntryFactory implements Factory<OutgoEntry> {

    private OutgoEntryService outgoEntryService;

    public Entry createOutgoEntry(final String name, final Category category, final Operator operator,
            final PaymentMethod paymentMethod, final BigDecimal value, final Date paymentDate,
            final PeriodType periodType) {
        final OutgoEntry outgoEntry = new OutgoEntryHibernate();
        outgoEntry.setName(name);
        outgoEntry.setCategory(category);
        outgoEntry.setOperator(operator);
        outgoEntry.setPaymentMethod(paymentMethod);
        outgoEntry.setPeriodType(periodType);
        outgoEntry.setValue(value);
        outgoEntry.setPaymentDate(paymentDate);
        return outgoEntry;
    }

    public Entry createDespesa(final String name, final Category category, final Operator operator,
            final PaymentMethod paymentMethod, final BigDecimal value, final Date paymentDate) {
        return createOutgoEntry(name, category, operator, paymentMethod, value, paymentDate, null);
    }

    @Override
    public OutgoEntry create() {
        return outgoEntryService.createEntity();
    }

}
