package org.bwit.factory;

import org.bwit.model.Entity;

public interface Factory<T extends Entity> {
	public T create();
}
