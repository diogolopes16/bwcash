package org.bwit.importer;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

import org.bwit.factory.EntityFactory;
import org.bwit.model.Account;
import org.bwit.model.Category;
import org.bwit.model.Entry;
import org.bwit.model.Operator;
import org.bwit.model.PaymentMethod;
import org.bwit.spring.configuration.TestCashConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestCashConfiguration.class)
public class ExcelImporterTest {

    private static final String FILE_NAME = "c://diogo//docs//extrato.xlsx";

    @Autowired
    private Importer importer;

    @Autowired
    private EntityFactory entityFactory;

    @Test
    public void testCreate() {
        final Category category = entityFactory.createCategory();
        final Operator operator = entityFactory.createOperator();
        final PaymentMethod paymentMethod = entityFactory.createPaymentMethod();
        final Account account = entityFactory.createAccount();

        System.out.println(category);
        System.out.println(operator);
        System.out.println(paymentMethod);
        System.out.println(account);
    }


    @Test
    public void testImport() {
        try (final InputStream inputStream = new FileInputStream(FILE_NAME);){
    		assertNotNull("Test file missing", inputStream);
    		final Collection<Entry> entries = importer.importFile(inputStream);

    		for (final Entry entry : entries) {
    			System.out.println(entry);
    		}

        } catch (final IOException e) {
        	fail("IOException " + FILE_NAME);
        }
    }
}
