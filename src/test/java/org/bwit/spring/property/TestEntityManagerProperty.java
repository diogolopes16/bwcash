package org.bwit.spring.property;

import org.apache.commons.lang3.StringUtils;
import org.bwit.spring.config.property.EntityManagerProperty;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component(value = "testEntityManagerProperty")
public class TestEntityManagerProperty implements EntityManagerProperty {

    @Value("${entitymanager.persistence.unitname:}")
    private String persistenceUnitName;

    @Value("${entitymanager.packages.to.scan:}")
    private String packagesToScan;

    public String[] getPackagesToScan() {
        return StringUtils.split(StringUtils.replace(packagesToScan, " ", ""), ",");
    }

    public String getPersistenceUnitName() {
        return persistenceUnitName;
    }

}
