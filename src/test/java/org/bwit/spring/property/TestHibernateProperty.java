package org.bwit.spring.property;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.bwit.spring.config.property.HibernateProperty;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.stereotype.Component;

@Component(value = "testHibernateProperty")
public class TestHibernateProperty implements HibernateProperty {

    @Value("${hibernate.default_schema:}")
    private String schema;

    @Value("${hibernate.default_catalog:}")
    private String catalog;

    @Value("${hibernate.database:}")
    private String dataBaseName;

    @Value("${hibernate.dialect:}")
    private String dialect;

    @Value("${hibernate.show_sql:}")
    private String showSQL;

    @Value("${hibernate.generate_ddl:}")
    private String generateDDL;

    @Value("${hibernate.format_sql:}")
    private String formatSQL;

    @Value("${hibernate.hbm2ddl.auto:}")
    private String hbm2DDL;

    @Value("${hibernate.max_fetch_depth:}")
    private String maxFetchDepth;

    @Value("${hibernate.connection.url:}")
    private String connectionUrl;

    @Value("${hibernate.connection.username:}")
    private String connectionUsername;

    @Value("${hibernate.connection.password:}")
    private String connectionPassword;

    @Value("${hibernate.connection.driver_class:}")
    private String connectionDriver;

    @Value("${hibernate.connection.release_mode:}")
    private String connectionReleaseMode;

    @Value("${hibernate.transaction.factory_class:}")
    private String transactionFactoryClass;

    @Value("${hibernate.transaction.manager_lookup_class:}")
    private String transactionManagerLookupClass;

    @Value("${hibernate.current_session_context_class:}")
    private String sessionContextClass;

    public String getDefaultSchema() {
        return schema;
    }

    public String getDefaultCatalog() {
        return catalog;
    }

    public Database getDatabase() {
        return StringUtils.isNotBlank(dataBaseName) ? Database.valueOf(dataBaseName) : Database.DEFAULT;
    }

    public String getDialect() {
        return dialect;
    }

    public Boolean isShowSql() {
        return BooleanUtils.toBoolean(showSQL);
    }

    public Boolean isGenerateDDL() {
        return BooleanUtils.toBoolean(generateDDL);
    }

    public Boolean isFormatSQL() {
        return BooleanUtils.toBoolean(formatSQL);
    }

    public String getHBM2DDL() {
        return hbm2DDL;
    }

    public String getMaxFetchDepth() {
        return maxFetchDepth;
    }

    public String getURL() {
        return connectionUrl;
    }

    public String getUsername() {
        return connectionUsername;
    }

    public String getPassword() {
        return connectionPassword;
    }

    public String getDriverClass() {
        return connectionDriver;
    }

    public String getTransactionalFacotry() {
        return transactionFactoryClass;
    }

    public String getTransactionalManagerLookup() {
        return transactionManagerLookupClass;
    }

    public String getCurrentSessionContext() {
        return sessionContextClass;
    }

    public String getConnectionReleaseMode() {
        return connectionReleaseMode;
    }

    public Map<String, String> getJPAPropertyMap() {
        final Map<String, String> jpaPropertyMap = new HashMap<String, String>();

        jpaPropertyMap.put("hibernate.dialect", dialect);
        jpaPropertyMap.put("hibernate.hbm2ddl.auto", hbm2DDL);
        jpaPropertyMap.put("hibernate.connection.driver_class", connectionDriver);
        jpaPropertyMap.put("hibernate.transaction.factory_class", transactionFactoryClass);
        jpaPropertyMap.put("hibernate.current_session_context_class", sessionContextClass);
        jpaPropertyMap.put("hibernate.connection.release_mode", connectionReleaseMode);
        return jpaPropertyMap;
    }

    public HibernateJpaVendorAdapter getJpaVendorAdapter() {
        final HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
        hibernateJpaVendorAdapter.setShowSql(isShowSql());
        hibernateJpaVendorAdapter.setGenerateDdl(isGenerateDDL());
        hibernateJpaVendorAdapter.setDatabase(getDatabase());
        hibernateJpaVendorAdapter.setDatabasePlatform(getDialect());
        return hibernateJpaVendorAdapter;
    }
}
