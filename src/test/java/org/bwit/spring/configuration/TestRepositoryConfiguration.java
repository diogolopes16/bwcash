package org.bwit.spring.configuration;

import java.io.IOException;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.bwit.spring.config.property.EntityManagerProperty;
import org.bwit.spring.config.property.HibernateProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Configure spring JPA application
 *
 * @author Carlos Belleza
 *
 */
@Configuration
@EnableTransactionManagement
public class TestRepositoryConfiguration {

    @Autowired
    @Qualifier("testHibernateProperty")
    private HibernateProperty hibernateProperty;

    @Autowired
    @Qualifier("testEntityManagerProperty")
    private EntityManagerProperty entityManagerProperty;

    /**
     * Get Portal datasource
     *
     * @return DataSource
     * @throws NamingException
     * @throws IOException
     */
    @Bean
    public DataSource portalDataSource() throws NamingException, IOException {
        final String connUrl = hibernateProperty.getURL();
        final String connUser = hibernateProperty.getUsername();
        final String connPass = hibernateProperty.getPassword();
        return new DriverManagerDataSource(connUrl, connUser, connPass);
    }

    /**
     * Get Portal entity manager
     *
     * @return LocalContainerEntityManagerFactoryBean
     * @throws NamingException
     * @throws IOException
     */
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws NamingException, IOException {
        final String[] entityManagerPackagesToScan = entityManagerProperty.getPackagesToScan();
        final String entityManagerPersistenceUnitName = entityManagerProperty.getPersistenceUnitName();
        final LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();

        entityManagerFactoryBean.setPersistenceUnitName(entityManagerPersistenceUnitName);
        entityManagerFactoryBean.setDataSource(portalDataSource());
        entityManagerFactoryBean.setPackagesToScan(entityManagerPackagesToScan);
        entityManagerFactoryBean.setJpaVendorAdapter(hibernateProperty.getJpaVendorAdapter());
        entityManagerFactoryBean.setJpaPropertyMap(hibernateProperty.getJPAPropertyMap());

        return entityManagerFactoryBean;
    }

    /**
     * Get Portal transaction manager
     *
     * @return JpaTransactionManager
     * @throws ClassNotFoundException
     * @throws NamingException
     * @throws IOException
     */
    @Bean
    public JpaTransactionManager transactionManager() throws ClassNotFoundException, NamingException, IOException {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        transactionManager.setNestedTransactionAllowed(true);
        return transactionManager;
    }
}