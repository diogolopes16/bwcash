package org.bwit.spring.configuration;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.naming.NamingException;

import org.bwit.spring.util.PropertySourcesConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.jndi.JndiTemplate;

@Configuration
@ComponentScan("org.bwit.spring.property")
public class TestPropertySourcesConfig {

    private final static String JNDI_ACRONYM_NAME = "java:global/env/AcronymName";
    private final static String JNDI_HOST_ALIAS_NAME = "java:global/env/HostAliasName";
    private final static String JNDI_CAS_FORWARD_APP_ADDRESS = "java:global/env/CasForwardAppAddress";
    private final static String JNDI_MESSAGE_SOURCE_CACHE_SECONDS = "java:global/env/MessageSourceCacheSeconds";

    @Bean
    public JndiTemplate jnditemplate() {
        final Properties properties = new Properties();
        properties.put("java.naming.factory.url.pkgs", "org.jboss.ejb.client.naming");

        final JndiTemplate jndiTemplate = new JndiTemplate();
        jndiTemplate.setEnvironment(properties);
        return jndiTemplate;
    }

    @Bean
    public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() throws IOException {
        // Get jndi variables from JBOSS
        final Map<String, Object> envJNDIMap = new HashMap<String, Object>();

        addJNDIContentToMap(envJNDIMap, "acronym.name", JNDI_ACRONYM_NAME);
        addJNDIContentToMap(envJNDIMap, "host.alias.name", JNDI_HOST_ALIAS_NAME);
        addJNDIContentToMap(envJNDIMap, "cas.forward.app.address", JNDI_CAS_FORWARD_APP_ADDRESS);
        addJNDIContentToMap(envJNDIMap, "ext.messagesource.cache.seconds", JNDI_MESSAGE_SOURCE_CACHE_SECONDS);

        final MapPropertySource envJNDIPropSource = new MapPropertySource("envJNDIMap", envJNDIMap);

        // Application properties
        final Properties prop = new Properties();
        prop.load(getClass().getClassLoader().getResourceAsStream("test-application.properties"));
        final PropertiesPropertySource propertySourceFile = new PropertiesPropertySource("test-application", prop);

        final MutablePropertySources mutablePropertySources = new MutablePropertySources();
        mutablePropertySources.addLast(envJNDIPropSource);
        mutablePropertySources.addLast(propertySourceFile);

        // Join into environment
        final PropertySourcesConfigurer configSourcesConfigurer = new PropertySourcesConfigurer();
        configSourcesConfigurer.setIgnoreResourceNotFound(true);
        configSourcesConfigurer.setIgnoreUnresolvablePlaceholders(true);
        configSourcesConfigurer.setPropertySources(mutablePropertySources);

        return configSourcesConfigurer;
    }

    private void addJNDIContentToMap(final Map<String, Object> map, final String key, final String variable) {
        try {
            final Object lookupObject = jnditemplate().lookup(variable);
            if (lookupObject != null) {
                map.put(key, lookupObject);
            }
        } catch (final NamingException e) {
            map.put(key, "");
        }
    }
}