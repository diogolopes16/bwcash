package org.bwit.spring.configuration;

import org.bwit.spring.config.StandardApplicationConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration(value = "testConfiguration")
@PropertySource("classpath:/test-application.properties")
@ComponentScan(basePackages = { "org.bwit.model", "org.bwit.importer", "org.bwit.factory", "org.bwit.service",
        "org.bwit.spring.config.property" })
@EnableJpaRepositories("org.bwit.repository")
@Import({ TestPropertySourcesConfig.class, StandardApplicationConfig.class, TestRepositoryConfiguration.class })
public class TestCashConfiguration {

}
